---
codename: 'suzu'
name: 'Sony Xperia X (F5121 & F5122)'
comment: 'community device'
icon: 'phone'
maturity: .8
---

### Preparatory steps

You can install Ubuntu Touch on the versions F5121 and F5122 of the Sony Xperia X.
Before, you have to take some preparatory steps:

1. Ensure you have [upgraded the stock firmware at least to Android 8](https://forums.ubports.com/topic/3229/sony-xperia-x-suzu-f5121).
2. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) in the Android settings.
3. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking.
4. **BEFORE unlocking the boot loader**, [back up your TA partition](https://together.jolla.com/question/168711/xperia-x-backup-ta-partition-before-unlocking-bootloader/), in case you later wish to return to factory state. **You can't do this step at a later stage!**!
5. [Get an unlocking code from Sony](https://developer.sony.com/develop/open-devices/get-started/unlock-bootloader/).
6. Reboot to fastboot and unlock the bootloader using the code obtained from Sony.

### Device specifications
|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm Snapdragon 650 MSM8956                                       |
|          CPU | 2x 1.8 GHz ARM Cortex-A72, 4x 1.2 GHz ARM Cortex-A53                  |
|          GPU | Adreno 510                                                            |
|      Display | 5 in, IPS, 1080 x 1920, 24 Bit                                        |
|      Storage | 32 GB / 64 GB                                                         |
| Shipped Android Version | 6.0.1                                                      |
|       Memory | 3 GB                                                                  |
|      Cameras | 5520 x 4140, 1920 x 1080, 30 fps |
|      Battery | 2620 mAh, Li-Polymer |
|   Dimensions | 69.4 x 142.7 x 7.9 mm |
|       Weight | 153 g |
|      MicroSD | microSD, microSDHC, microSDXC |
| Release Date | June 2016 |

### Maintainer(s)

- [fredldotme](https://forums.ubports.com/user/fredldotme)

### Forum topic

- https://forums.ubports.com/topic/3229/sony-xperia-x-suzu-f5121-f5122

#### Kernel

- https://github.com/fredldotme/device-kernel-loire

#### Device

- https://github.com/fredldotme/device-sony-common
- https://github.com/fredldotme/device-sony-loire
- https://github.com/fredldotme/device-sony-suzu

#### Halium

- https://github.com/Halium/halium-devices/blob/halium-7.1/manifests/sony_suzu.xml
