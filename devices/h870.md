---
codename: 'h870'
name: 'LG G6'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: .6
---

### Preparatory steps

I experienced to install Ubuntu Touch on LG G6 (h870). There are many others I can not test on, such h872, LS993, VS998, ...
Before, you have to take a number of preparatory steps:

1. Ensure you have upgraded the stock firmware at least to Android 7.1, else you can't flash the OEM binaries for AOSP. It is best to ensure that you have all the latest firmware installed.
2. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/)  for your device,
3. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking,
4. Install ADB on your computer
5. Get an unlocking code from [LG](http://mobile.developer.lge.com) 
6. Reboot to fastboot and unlock the bootloader using the code obtained from LG
7. Flash it in fastboot (`fastboot flash unlock unlock.bin`)

### Device specifications
|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8996 Snapdragon 821                                       |
|          CPU | Octa-core Cortex-A53 & Cortex-A574 x 2.0 Ghz + 4 x 1.5 Ghz            |
| Architecture | arm64                                                                 |
|          GPU | Adreno 430                                                            |
|      Display | 5.7 in (14.47cm), Fullvision Quad HD+ (2880x1440) 18:9                |
|      Storage | 32 GB                                                                 |
| Shipped Android Version | 7.0                                                        |
|       Memory | 4 GB                                                                  |
|      Cameras | Double camera 13 MP F/1.8 LED flash, 5MP f2.2                         |
|      Battery | Non-removable Li-Ion 3.300 mAh                                        |
|   Dimensions | 148.9 x 71.9 x 7.9 mm                                                 |
|       HDR    | HDR10 & Dolby Vision™                                                 |
|      MicroSD | microSD, microSDHC, microSDXC up to 2TB                               |
| Release Date | 2017                                                                  |
|          SIM | nano                                                                  |

### Maintainer(s)

- [guf](https://forums.ubports.com/user/guf)

### Forum topic

- https://forums.ubports.com/topic/3729/ubuntu-touch-on-lg-g6-h870-model-call-for-testers

### Source repos
- https://github.com/LG-G6-DEV/lge_msm8996
- https://github.com/LG-G6-DEV/android_kernel_lge_msm8996
- https://github.com/LG-G6-DEV/android_device_lge_g6-common
- https://github.com/LG-G6-DEV/android_device_lge_msm8996-common
- https://github.com/LG-G6-DEV/android_device_lge_h870
- https://github.com/LG-G6-DEV/android_device_lge_h870ds
- https://github.com/LG-G6-DEV/android_device_lge_us997
- https://github.com/LG-G6-DEV/android_device_lge_h872

#### Kernel

- https://github.com/rymdllama/android_kernel_lge_msm8996

#### Device

- https://github.com/rymdllama/android_device_lge_h870
- https://github.com/rymdllama/android_device_lge_g6-common
- https://github.com/rymdllama/android_device_lge_msm8996-common

#### Halium
- https://github.com/Halium/projectmanagement/issues/115

- https://github.com/Halium/halium-devices/blob/halium-7.1/manifests/lge_h870.xml

#### LOS
- https://github.com/LineageOS/android_device_lge_h870