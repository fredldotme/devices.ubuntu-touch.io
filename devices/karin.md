---
name: 'Sony Xperia Z4 Tablet (SGP771 & SGP712)'
codename: 'karin'
comment: 'community device'
icon: 'tablet'
maturity: .6
---

### Preparatory steps

You can install Ubuntu Touch on the versions SGP771 of the Sony Xperia Z4 Tablet (karin) LTE and SGP712, Sony Xperia Z4 Tablet (karin_windy) Wifi only.
Before, you have to take a number of preparatory steps:

1. Ensure you have upgraded the stock firmware at least to Android 7.1, else you can't flash the OEM binaries for AOSP. It is best to ensure that you have all the latest firmware installed.
2. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/)  for your device,
3. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking,
4. Install ADB on your computer
5. **BEFORE unlocking the boot loader**, [back up your TA partition](https://together.jolla.com/question/168711/xperia-x-backup-ta-partition-before-unlocking-bootloader/), in case you later wish to return to factory state. **You can't do this step at a later stage!**
6. [Get an unlocking code from Sony](https://developer.sony.com/develop/open-devices/get-started/unlock-bootloader/),
7. Reboot to fastboot and unlock the bootloader using the code obtained from Sony
8. Download the [OEM binaries for AOSP from sony](https://developer.sony.com/file/download/software-binaries-for-aosp-marshmallow-android-6-0-1-legacy/), unpack them,
9. Flash them in fastboot (`fastboot flash oem [filename]`)

### Device specifications
|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8994 Snapdragon 810                                       |
|          CPU | Octa-core Cortex-A53 & Cortex-A574 x 2.0 Ghz + 4 x 1.5 Ghz            |
| Architecture | arm64                                                                 |
|          GPU | Adreno 430                                                            |
|      Display | 10,1 in, IPS, 2560x1600 (299 PPI)                                     |
|      Storage | 16 GB / 32 GB                                                         |
| Shipped Android Version | 6.0.1                                                      |
|       Memory | 3 GB                                                                  |
|      Cameras | 8.1 MP F/2.0 LED flash, 5.1 MP F/2.4 no flash, 30 fps                 |
|      Battery | Non-removable Li-Ion 6000 mAh                                         |
|   Dimensions | 167 mm (6.57 in) (h) x 254 mm (10 in) (w) x 6.1 mm (0.24 in) (d)      |
|       Weight |                                                                       |
|      MicroSD | microSD, microSDHC, microSDXC                                         |
| Release Date | October 2015                                                          |
|          SIM | nano                                                                  |

### Maintainer(s)

- [guf](https://forums.ubports.com/user/guf)

### Forum topic

### Source repos
- https://github.com/sonyxperiadev/device-sony-karin
- https://github.com/sonyxperiadev/device-sony-karin_windy

#### Kernel

- https://github.com/fredldotme/device-kernel-loire

#### Device

- https://github.com/ubports/android_device_sony_kitakami-common
- https://github.com/ubports/android_device_sony_karin
- https://github.com/ubports/android_device_sony_karin_windy

#### Halium
- https://github.com/Halium/projectmanagement/issues/44

- https://github.com/Halium/halium-devices/blob/halium-7.1/manifests/sony_karin.xml
- https://github.com/Halium/halium-devices/blob/halium-7.1/manifests/sony_karin_windy.xml
