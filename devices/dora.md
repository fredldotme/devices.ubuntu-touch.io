---
codename: 'dora'
name: 'Sony Xperia X Performance (F8131 & F8132)'
comment: 'community device'
icon: 'phone'
maturity: .8
---

You can install Ubuntu Touch on the versions F8131 and F8132 of the Sony Xperia X Peformance. Further details on installing can be found at. Phone preparation steps can be found in the [devices](https://forums.ubports.com/topic/4147/sony-xperia-x-performance-dora-f8131-f8132) forum
